package com.ndnwork.loancalculator;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private static String EMPTY_STRING = "";
    private static String LOAN_TIME_HEADER = "Thời gian vay";
    private static String LOAN_TYPE_HEADER = "Loại hình vay";
    private static String LOAN_DECREASING_TYPE = "Dư nợ giảm dần";
    private static String LOAN_INITIAL_TYPE = "Dư nợ ban đầu";

    private final Context context = this;
    private EditText loanAmountEditText;
    private Spinner loanTimeSpinner;
    private EditText interestRateEditText;
    private Spinner loanTypeSpinner;
    private TableLayout summaryReportTable;
    private TableLayout detailsReportTable;
    private Button loanCalculatorButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                /* Fill it with Data */
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"nguyendangnghiem@gmail.com"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report on 'TÍNH TIỀN VAY' application");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hello Nghiem Nguyen,");

                /* Send it off to the Activity-Chooser */
                context.startActivity(Intent.createChooser(emailIntent, "Choose an Email client"));

                /*
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                */
            }
        });


        // Loan Amount Section
        loanAmountEditText = (EditText) findViewById(R.id.loanAmount);
        // loanAmountEditText.setOnClickListener(this);

        // add text changed listener
        loanAmountEditText.addTextChangedListener(new TextWatcher() {
            // int currentCursor;
            // http://stackoverflow.com/questions/5107901/better-way-to-format-currency-input-edittext
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && !"".equals(s.toString())) {
                    loanAmountEditText.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[,]", "");

                    long parsed = Long.parseLong(cleanString);
                    String formatted = NumberFormat.getInstance().format(parsed);

                    loanAmountEditText.setText(formatted);
                    loanAmountEditText.setSelection(formatted.length());

                    loanAmountEditText.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO handle current cursor
                // loanAmountEditText.setSelection(currentCursor);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO handle current cursor
                // currentCursor = loanAmountEditText.getSelectionStart();
            }
        });
        // End Loan Amount Section

        // Loan Time Section
        loanTimeSpinner = (Spinner) findViewById(R.id.loanTime);
        String[] loanTimeItems = new String[121];
        loanTimeItems[0] = LOAN_TIME_HEADER;
        for (int i = 1; i <= 120; i++) {
            loanTimeItems[i] = String.valueOf(i);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, loanTimeItems);
        loanTimeSpinner.setAdapter(adapter);
        // End Loan Time Section

        // Start Interest Rate
        interestRateEditText = (EditText) findViewById(R.id.interestRate);
        // interestRateEditText.setOnClickListener(this);
        // add text changed listener
        interestRateEditText.addTextChangedListener(new TextWatcher() {
            String beforeChangedValue = "";

            // http://stackoverflow.com/questions/5107901/better-way-to-format-currency-input-edittext
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && !"".equals(s.toString())) {
                    interestRateEditText.removeTextChangedListener(this);

                    String inputValue = s.toString();
                    float parsed = Float.parseFloat(inputValue);

                    if (parsed > 100) {
                        interestRateEditText.setText(beforeChangedValue);
                        interestRateEditText.setSelection(beforeChangedValue.length());
                    }

                    interestRateEditText.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO handle current cursor
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                beforeChangedValue = s.toString();
            }
        });
        // End Interest Rate Section

        // Loan Type Section
        loanTypeSpinner = (Spinner) findViewById(R.id.loanType);
        String[] loanTypeItems = new String[]{LOAN_TYPE_HEADER, LOAN_DECREASING_TYPE, LOAN_INITIAL_TYPE};

        ArrayAdapter<String> loanTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, loanTypeItems);
        loanTypeSpinner.setAdapter(loanTypeAdapter);
        // End Loan Type Section


        // summary report Section
        summaryReportTable = (TableLayout) findViewById(R.id.summaryReportTable);
        // End summary report Section

        // details report Section
        detailsReportTable = (TableLayout) findViewById(R.id.detailsReportTable);
        // End details report Section


        // Button Action Section
        loanCalculatorButton = (Button) findViewById(R.id.loanCalculatorButton);
        loanCalculatorButton.setOnClickListener(this);
        // End Button Action Section
    }

    private boolean isValidInputs(String loadAmount, String loanTime, String interestRate, String loanType) {
        if (EMPTY_STRING.equals(loadAmount) ||
                LOAN_TIME_HEADER.equalsIgnoreCase(loanTime) ||
                EMPTY_STRING.equals(interestRate) ||
                LOAN_TYPE_HEADER.equalsIgnoreCase(loanType)) {
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.loanCalculatorButton:
                // get all input values
                String loadAmountStr = loanAmountEditText.getText().toString();
                String loanTimeStr = loanTimeSpinner.getSelectedItem().toString();
                String interestRateStr = interestRateEditText.getText().toString();
                String loanTypeStr = loanTypeSpinner.getSelectedItem().toString();

                if (!isValidInputs(loadAmountStr, loanTimeStr, interestRateStr, loanTypeStr)) {
                    // http://www.mkyong.com/android/android-alert-dialog-example/
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                    // set title
                    alertDialogBuilder.setTitle("Thông báo");

                    // set dialog message
                    AlertDialog.Builder no = alertDialogBuilder
                            .setMessage("Bạn cần nhập / chọn đầy đủ tất cả thông thông tin!")
                            .setCancelable(false)
                            .setNegativeButton("ĐÓNG", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    long loanAmount = Long.parseLong(loadAmountStr.replaceAll("[,]", ""));
                    int loanTime = Integer.parseInt(loanTimeStr);
                    float interestRate = Float.parseFloat(interestRateStr);

                    long totalPaid = 0l;
                    List<PaidItem> paidItems = new ArrayList<PaidItem>();
                    for (int i = 1; i <= loanTime; i++) {
                        long loanAmountEachMonth = loanAmount / loanTime;
                        long debtOfBegin = loanAmount - loanAmountEachMonth * (i - 1);

                        long interestAmountEachMonth = 0l;
                        if (LOAN_DECREASING_TYPE.equalsIgnoreCase(loanTypeStr)) {
                            interestAmountEachMonth = (long) (interestRate / 12 / 100 * debtOfBegin);
                        } else {
                            interestAmountEachMonth = (long) (interestRate / 12 / 100 * loanAmount);
                        }

                        totalPaid += loanAmountEachMonth + interestAmountEachMonth;

                        // create list paid items
                        paidItems.add(new PaidItem(i, debtOfBegin, loanAmountEachMonth, interestAmountEachMonth));

                    }

                    // summary report
                    createSummaryReport(loanAmount, totalPaid);

                    // detail report
                    createDetailsReport(paidItems);
                }
                break;
        }

    }

    private void createSummaryReport(long loanAmount, long totalPaid) {
        // reset table
        summaryReportTable.removeAllViews();

        // report header
        TableRow headerRow = new TableRow(this);
        TableRow.LayoutParams headerRowParams = new CustomLayoutParams(2);
        headerRow.setLayoutParams(headerRowParams);
        headerRow.setBackgroundColor(Color.GRAY);

        TextView headerTitle = new CustomTextView(this, "THỐNG KÊ TIỀN VAY");
        TableRow.LayoutParams headerTitleParams = new CustomLayoutParams(2);
        headerTitle.setLayoutParams(headerTitleParams);

        headerRow.addView(headerTitle);
        summaryReportTable.addView(headerRow);

        // total paid row
        TableRow totalPaidRow = new TableRow(this);
        TableRow.LayoutParams totalPaidRowParams = new CustomLayoutParams(2);
        totalPaidRow.setLayoutParams(totalPaidRowParams);

        TextView totalPaidTitle = new CustomTextView(this, "Tổng phải trả");
        TableRow.LayoutParams totalPaidTitleParams = new CustomLayoutParams();
        totalPaidTitle.setLayoutParams(totalPaidTitleParams);

        TextView totalPaidValue = new CustomTextView(this, this.formatCurrency(totalPaid, "đ"));
        TableRow.LayoutParams totalPaidValueParams = new CustomLayoutParams();
        totalPaidValue.setLayoutParams(totalPaidValueParams);
        totalPaidValue.setGravity(Gravity.RIGHT);

        totalPaidRow.addView(totalPaidTitle);
        totalPaidRow.addView(totalPaidValue);

        // loan amount row
        TableRow loanAmountRow = new TableRow(this);
        TableRow.LayoutParams loanAmountRowParams = new CustomLayoutParams(2);
        loanAmountRow.setLayoutParams(loanAmountRowParams);

        TextView loanAmountTitle = new CustomTextView(this, "Gốc");
        TableRow.LayoutParams loanAmountTitleParams = new CustomLayoutParams();
        loanAmountTitle.setLayoutParams(loanAmountTitleParams);

        TextView loanAmountValue = new CustomTextView(this, this.formatCurrency(loanAmount, "đ"));
        TableRow.LayoutParams loanAmountValueParams = new CustomLayoutParams();
        loanAmountValue.setLayoutParams(loanAmountValueParams);
        loanAmountValue.setGravity(Gravity.RIGHT);

        loanAmountRow.addView(loanAmountTitle);
        loanAmountRow.addView(loanAmountValue);

        // total interest row
        TableRow totalInterestRow = new TableRow(this);
        TableRow.LayoutParams totalInterestRowParams = new CustomLayoutParams(2);
        totalInterestRow.setLayoutParams(totalInterestRowParams);

        TextView totalInterestTitle = new CustomTextView(this, "Lãi");
        TableRow.LayoutParams totalInterestTitleParams = new CustomLayoutParams();
        totalInterestTitle.setLayoutParams(totalInterestTitleParams);

        TextView totalInterestValue = new CustomTextView(this, this.formatCurrency(totalPaid - loanAmount, "đ"));
        TableRow.LayoutParams totalInterestValueParams = new CustomLayoutParams();
        totalInterestValue.setLayoutParams(totalInterestValueParams);
        totalInterestValue.setGravity(Gravity.RIGHT);

        totalInterestRow.addView(totalInterestTitle);
        totalInterestRow.addView(totalInterestValue);

        // add all rows to table
        summaryReportTable.addView(totalPaidRow);
        summaryReportTable.addView(loanAmountRow);
        summaryReportTable.addView(totalInterestRow);
    }

    private void createDetailsReport(List<PaidItem> paidItems) {
        // reset table
        detailsReportTable.removeAllViews();

        // report header
        TableRow headerRow = new TableRow(this);
        TableRow.LayoutParams headerRowParams = new CustomLayoutParams(3);
        headerRow.setLayoutParams(headerRowParams);
        headerRow.setBackgroundColor(Color.GRAY);

        TextView header1Title = new CustomTextView(this, "KỲ");
        TableRow.LayoutParams header1TitleParams = new CustomLayoutParams();
        header1Title.setLayoutParams(header1TitleParams);
        header1Title.setGravity(Gravity.CENTER);

        TextView header2Title = new CustomTextView(this, "NỢ ĐẦU KỲ");
        TableRow.LayoutParams header2TitleParams = new CustomLayoutParams();
        header2Title.setLayoutParams(header2TitleParams);
        header2Title.setGravity(Gravity.RIGHT);

        TextView header3Title = new CustomTextView(this, "PHẢI TRẢ");
        TableRow.LayoutParams header3TitleParams = new CustomLayoutParams();
        header3Title.setLayoutParams(header3TitleParams);
        header3Title.setGravity(Gravity.RIGHT);

        headerRow.addView(header1Title);
        headerRow.addView(header2Title);
        headerRow.addView(header3Title);

        detailsReportTable.addView(headerRow);

        // loop list to add all rows
        int i = 0;
        for (PaidItem item : paidItems) {
            TableRow itemRow = new TableRow(this);
            TableRow.LayoutParams itemRowParams = new CustomLayoutParams(3);
            itemRow.setLayoutParams(itemRowParams);

            TextView cell1Title = new CustomTextView(this, String.valueOf(++i));
            TableRow.LayoutParams cell1TitleParams = new CustomLayoutParams();
            cell1Title.setLayoutParams(cell1TitleParams);
            cell1Title.setGravity(Gravity.CENTER);

            TextView cell2Title = new CustomTextView(this, this.formatCurrency(item.getDebtOfBegin(), "đ"));
            TableRow.LayoutParams cell2TitleParams = new CustomLayoutParams();
            cell2Title.setLayoutParams(cell2TitleParams);
            cell2Title.setGravity(Gravity.RIGHT);

            TextView cell3Title = new CustomTextView(this, this.formatCurrency(item.getLoanAmountEachMonth() + item.getInterestAmountEachMonth(), "đ"));
            TableRow.LayoutParams cell3TitleParams = new CustomLayoutParams();
            cell3Title.setLayoutParams(cell3TitleParams);
            cell3Title.setGravity(Gravity.RIGHT);

            itemRow.addView(cell1Title);
            itemRow.addView(cell2Title);
            itemRow.addView(cell3Title);
            detailsReportTable.addView(itemRow);
        }

    }

    private String formatCurrency(long value, String unit) {
        return NumberFormat.getInstance().format(value) + unit;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
