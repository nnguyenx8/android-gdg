package com.ndnwork.loancalculator;

public class PaidItem {
    private int no;
    private long debtOfBegin;
    private long loanAmountEachMonth;
    private long interestAmountEachMonth;

    public PaidItem(int no, long debtOfBegin, long loanAmountEachMonth, long interestAmountEachMonth) {
        this.no = no;
        this.debtOfBegin = debtOfBegin;
        this.loanAmountEachMonth = loanAmountEachMonth;
        this.interestAmountEachMonth = interestAmountEachMonth;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public long getDebtOfBegin() {
        return debtOfBegin;
    }

    public void setDebtOfBegin(long debtOfBegin) {
        this.debtOfBegin = debtOfBegin;
    }

    public long getLoanAmountEachMonth() {
        return loanAmountEachMonth;
    }

    public void setLoanAmountEachMonth(long loanAmountEachMonth) {
        this.loanAmountEachMonth = loanAmountEachMonth;
    }

    public long getInterestAmountEachMonth() {
        return interestAmountEachMonth;
    }

    public void setInterestAmountEachMonth(long interestAmountEachMonth) {
        this.interestAmountEachMonth = interestAmountEachMonth;
    }
}
