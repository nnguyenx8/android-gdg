package com.ndnwork.loancalculator;

import android.widget.TableRow;

public class CustomLayoutParams extends TableRow.LayoutParams {
    public CustomLayoutParams() {
        super(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
    }

    public CustomLayoutParams(int span) {
        super(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        this.span = span;
    }

    public CustomLayoutParams(int w, int h) {
        super(w, h);
    }

    public CustomLayoutParams(int w, int h, int span) {
        super(w, h);
        this.span = span;
    }
}
