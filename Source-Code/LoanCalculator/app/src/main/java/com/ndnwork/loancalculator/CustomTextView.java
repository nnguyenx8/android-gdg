package com.ndnwork.loancalculator;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TableRow;
import android.widget.TextView;

public class CustomTextView extends TextView {

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, String text) {
        super(context);
        setText(text);
    }

    public CustomTextView(Context context, String text, TableRow.LayoutParams layoutParams) {
        super(context);
        setText(text);
        setLayoutParams(layoutParams);
    }

}
