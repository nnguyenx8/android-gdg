### Introduction ###

* App: Loan Calculator (VN: Tính Tiền Vay)
* Author: Nguyen Dang Nghiem (nguyendangnghiem@gmail.com)

### Function ###
* Calculate the final amount (with detail report) a person need to pay when loaning money with specific interest rate & time
* Be able to use for Vietnamese only

### Screenshots ###

![screen-1.jpg](https://bitbucket.org/repo/8kLXrE/images/846747667-screen-1.jpg) ![screen-2.jpg](https://bitbucket.org/repo/8kLXrE/images/1491522767-screen-2.jpg)